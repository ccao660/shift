﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerFacialAnimation : MonoBehaviour {

    

    public void PlayHappyAnimation()
    {
        GetComponent<Animator>().SetTrigger("HAPPY");
    }

    public void PlayAngryAnimation()
    {
        GetComponent<Animator>().SetTrigger("ANGRY");
    }

    public void PlaySadAnimation()
    {
        GetComponent<Animator>().SetTrigger("SAD");
    }

    public void PlayKissAnimation()
    {
        GetComponent<Animator>().SetTrigger("KISS");
    }

    public void PlayWrikleAnimation()
    {
        GetComponent<Animator>().SetTrigger("WRIKLE");
    }


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
