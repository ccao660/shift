﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(OscOut))]

public class OscSendOut : MonoBehaviour {
    OscOut oscOut;
    private float[] DevicesOnePositionXYZ;
    private float[] DevicesOneRotationXYZ;
    // Use this for initialization
    void Start () {
        oscOut = gameObject.GetComponent<OscOut>();

        DevicesOnePositionXYZ = new float[3];
        DevicesOneRotationXYZ = new float[3];

    }

    // Update is called once per frame
    void Update() {

        DevicesOnePositionXYZ[0] = this.transform.position.x;
        DevicesOnePositionXYZ[1] = this.transform.position.y;
        DevicesOnePositionXYZ[2] = this.transform.position.z;


        DevicesOnePositionXYZ[0] = (float)Math.Round((double)DevicesOnePositionXYZ[0], 2);
        DevicesOnePositionXYZ[1] = (float)Math.Round((double)DevicesOnePositionXYZ[1], 2);
        DevicesOnePositionXYZ[2] = (float)Math.Round((double)DevicesOnePositionXYZ[2], 2);

        DevicesOneRotationXYZ[0] = this.transform.eulerAngles.x;
        DevicesOneRotationXYZ[1] = this.transform.eulerAngles.y;
        DevicesOneRotationXYZ[2] = this.transform.eulerAngles.z;


        OscMessage messagePostion = new OscMessage("/unity/1/position");
        messagePostion.Add(DevicesOnePositionXYZ[0].ToString() + ";"+ DevicesOnePositionXYZ[1].ToString() + ";"+(DevicesOnePositionXYZ[2].ToString()));

        oscOut.Send(messagePostion);
        
       // Debug.Log(messagePostion);
        OscMessage messageRotation = new OscMessage("/unity/1/rotation");
        messageRotation.Add(DevicesOneRotationXYZ[0].ToString() + ";" + DevicesOneRotationXYZ[1].ToString() + ";" + (DevicesOneRotationXYZ[2].ToString()));

        oscOut.Send(messageRotation);
//Debug.Log(messageRotation);
    }
}
